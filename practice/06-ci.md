Создаем 2 текстовых файла file1.txt, file2.txt.

Конфиг .gitlab.ci.yml

```yml
stages:
  - env
  - check
  - build

before_script:
  - apt update
  - apt install maven -y

check_env:
  stage: env
  image: ${CI_REGISTRY}/${GITLAB_USER_LOGIN}/${CI_PROJECT_NAME}/simple_cont
  tags:
    - docker-atp
  script: 
    env
  only:
    - master

check:
  image: alpine
  stage: check
  allow_failure: true
  tags:
    - docker-atp
  script:
    - df
    - free -h
    - nproc

check_file:
  stage: check
  script:
    cat file1.txt | grep -q "Hello world"

package:
  stage: build
  script:
    cat file1.txt file2.txt | gzip > my_package.gz
  artifacts:
    paths:
      - my_package.gz
    expire_in: 5 minutes
```

* [Начальный tutorial по Gitlab CI](https://about.gitlab.com/blog/2016/07/29/the-basics-of-gitlab-ci/).
* [Репозиторий с кодами семинара](http://gitlab.atp-fivt.org/root/tpos-gitlab-ci)
