#! /usr/bin/env python3

import os
import helloworld
from setuptools import setup, find_packages


def read_description():
    abs_path = os.path.join(os.path.dirname(__file__), "README.rst")
    with open(abs_path) as f:
        return f.read()


setup(
    name='helloworld',
    version=helloworld.__version__,
    packages=find_packages(),
    include_package_data=True,
    test_suite='helloworld.tests',
    install_requires=['Flask~=1.1'],
    description='Hello proj',
    long_description=read_description(),
    entry_points={'console_scripts': [
        'helloworld = helloworld.main:print_message',
        'web = helloworld.fl:run_server'
        ]
    },
    python_requires=">=3.4"
)
